function [X] = ExponentielleStable(A, B, iterations, deltaTemps, epsilon)
tic
U = B;
saut = expm(A*deltaTemps);

Uprecedent = ones(1600,1);
n = 0;

while (n<iterations) && ((max(abs(Uprecedent-U)))>epsilon),
    Uprecedent = U;
    U = saut* U;
    n = n+1;
end

disp([ ' Stabilit� trouv�e au rang : ' , num2str(n)]);
disp([ 'Temp�rature moyenne : ' ,  num2str(mean(mean(U)))]);

S = reshape(U,[16,100]);
surf(S)
axis([0 100 0 16 0 100]);
toc
end