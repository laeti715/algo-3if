
%% Pour le calcul matriciel classique
[A ,S,B] = carte();
%X = Jacobi(A,B,100); 
X = JacobiAnimation(A,B,50);
R = reshape(X,[16,100]);


%% Pour le calcul exponentiel
ExponentielleAnimation(A, B, 1000, 0.01);
ExponentielleEquilibre(A, B, 100000);
ExponentielleStable(A, B, 100000, 1, 0.000001);

% ExponentielleStable(A, B, 100000, 1, 0.0001)
%  Stabilit� trouv�e au rang : 950
% Elapsed time is 3.044346 seconds.
% ExponentielleStable(A, B, 100000, 0.1, 0.0001)
%  Stabilit� trouv�e au rang : 2099
% Elapsed time is 4.727016 seconds.
% ExponentielleStable(A, B, 100000, 0.001, 0.0001)
%  Stabilit� trouv�e au rang : 16443
% Elapsed time is 30.193672 seconds.