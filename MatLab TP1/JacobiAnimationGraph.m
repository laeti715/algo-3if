function [X] = JacobiAnimationGraph(A, B, iterations, epsilon)
%m�thode Jacobi
%[~, M, ~] = decompos(A); A utiliser pour la m�thode iterative avec
%matrices
taille = size(A);
Xprecedent = ones(taille(1),1);
X = zeros(taille(1),1);
Xsuivant = zeros(taille(1),1);

somme = 0;
n = 0;

while (n<iterations) && ((max(abs(Xprecedent-X)))>epsilon),
    for i=1:taille(1),
        for j=1:taille(1),
            if j~=i
                somme = somme + A(i, j)*X(j, 1);
            end
        end
        
        Xsuivant(i, 1) = B(i, 1) - somme;
        Xsuivant(i, 1) =  Xsuivant(i, 1)/A(i,i);
        somme = 0;
    end
    Xprecedent = X;
    X = Xsuivant;
    n = n+1;
end

disp([ ' Stabilit� trouv�e au rang : ' , num2str(n)]);
S = reshape(X,[16,100]);
surf(S);

end