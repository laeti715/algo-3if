function [X] = Jacobi(A, B, iterations)
%m�thode Jacobi
%[~, M, ~] = decompos(A); A utiliser pour la m�thode iterative avec
%matrices
taille = size(A);
Xsuivant = zeros(taille(1),1);
X = zeros(taille(1),1);
somme = 0;

for n=1:iterations
    for i=1:taille(1),
        for j=1:taille(1),
            if j~=i
                somme = somme + A(i, j)*X(j, 1);
            end
        end
        
        Xsuivant(i, 1) = B(i, 1) - somme;
        Xsuivant(i, 1) =  Xsuivant(i, 1)/A(i,i);
        somme = 0;
    end
    X = Xsuivant;
end

end