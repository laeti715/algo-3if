function [X] = ExponentielleAnimationCST(A, B, iterations, deltaTemps)
tic

taille = size(A);
U = B;
saut = expm(A*deltaTemps);
S = reshape(U,[16,100]);
surf(S)
axis([0 100 0 16 0 100]);
%% Calcul des lieux des 100�c
lignes = 16;
colonnes = 100;
IndexCentDegresInf = round(lignes*colonnes*0.8+0.6*lignes);
IndexCentDegresSup = round(lignes*colonnes*0.81+0.6*lignes);

%% Iterations pour animation
for n=1:iterations
    U = saut* U;
    
    S = reshape(U,[16,100]);
    surf(S)
    axis([0 100 0 16 0 100]);
    M(n)=getframe;
    % remise � 100�C
    U(IndexCentDegresInf, 1) = 100;
    U(IndexCentDegresInf+1, 1) = 100;
    U(IndexCentDegresSup, 1) = 100;
    U(IndexCentDegresSup+1, 1) = 100;
end

movie(M,2,1);
toc
end