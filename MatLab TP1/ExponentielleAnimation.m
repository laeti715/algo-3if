function [X] = ExponentielleAnimation(A, B, iterations, deltaTemps)
tic
taille = size(A);
U = B; 
saut = expm(A*deltaTemps);
S = reshape(U,[16,100]);
surf(S)
axis([0 100 0 16 0 100]);

for n=1:iterations
    U = saut* U;
    S = reshape(U,[16,100]);
    surf(S)
    axis([0 100 0 16 0 100]);
    M(n)=getframe;
end

movie(M,2,1);
toc
end