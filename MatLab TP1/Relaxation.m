function [X, omega] = Relaxation(A, B, iterations)
%m�thode Relaxation
[L, D, U] = decompos(A);
M = D+L;
N = M - A;

%trouver omega avec convergence
omega=10^-2;
%Premiere it�ration sans tests
matPi = inv(D+omega*L)*((1-omega)*D-omega*U);
rayonSpectralCool = abs(max(eig(matPi)))
 % les autres
for omegaCourant=10^-2:10^-2:2-10^-2,
    matPi = inv(D+omegaCourant*L)*((1-omegaCourant)*D-omegaCourant*U);
    rayonSpectral = abs(max(eig(matPi)));
    if rayonSpectralCool >rayonSpectral
        rayonSpectralCool = rayonSpectral;
        omega = omegaCourant;
    end
end
    
% Gauss : matPi = inv(M)*N;
matPi = inv(D+omega*L)*((1-omega)*D-omega*U); 
beta = inv(M)*B;

taille = size(A);
X = zeros(taille(1),1);

for n=1:iterations
    Xsuivant = matPi*X + beta;
    X = Xsuivant;
end

end