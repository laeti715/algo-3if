function [X] = ExponentielleEquilibre(A, B, temps)

saut = expm(A*temps);
U = saut*B;

S = reshape(U,[16,100]);
surf(S)
axis([0 100 0 16 0 100]);

end