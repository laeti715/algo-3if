function [valeurPropre, v, u] = PuissanceIteree(A)

taille = size(A);
erreur = 5; %10^-erreur pour calcul de la colinearite
X = ones(taille(1), 1);
v = ones(taille(1), 1);
u = ones(1, taille(1));

%% �viter les premi�res it�rations o� u,v et X sont colin�aires
v(:,1) = 2;
v(1,1) = 1;
u(1,:) = 2;
u(1,1) = 1;

%% On calcule le V
while ~isColineaireVertical(X, v, erreur) %tant que non colineaire
    X = v/norm(v);
    v = A*X;
end

% On r�cup�re une des valeurs de la division du vecteur v par X, donc
% le coefficient de proportionalit� /!\ Le X peut �tre nul, donc la
% division peut donner NaN, on choisit donc une valeur qui n'est pas
% avec un X nul.
% = on r�cup�re la premi�re valeur propre non nulle
for i=1:taille(1)
    if X(i) ~= 0
        valeurPropre = v(i)/X(i);
        break
    end
end

% DEBUG % disp([ ' valeur propre trouv�e ', num2str(valeurPropre)]);

%% On calcule le U
X = ones(1, taille(1));
while ~isColineaireHorizontal(X, u, erreur) %tant que non colineaire
    X = u/norm(u);
    u = X*A;
end

end