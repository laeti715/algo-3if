%% On consid�re une matrice. "-" car c'est -Lambda dans la formule
A = -tambour();

%% On d�marre le compteur de temps g�n�ral
tic

%% On pr�apre quelques variables utiles pour les performances
taille = size(A);
ValeurPropre = zeros(taille(1),1);
VecteurPropreDebut = zeros(taille(1),21);
VecteurPropreFin = zeros(taille(1),50);

%% On r�cup�re les 20 premi�res valeurs et vecteurs propres (les plus grands)
for i=1:20
    tic
    [ValeurPropre(i), v, u] = PuissanceIteree(A);
    VecteurPropreDebut(:,i) = v;
    A = DeflationWielandt(A, ValeurPropre(i), v, u);
    % DEBUG % 
    disp([ ' valeur propre trouv�e ', num2str(ValeurPropre(i)) , ' N� ', num2str(i)]);
    toc
end

%% On r�cup�re les 50 derni�res valeurs et vecteurs propres (les plus petits)
% On inverse la matrice (voir m�thode fiche TP2)
B = inv(A);

for i=1:40
    tic
    [ValeurPropre(i), v, u] = PuissanceIteree(B);
    VecteurPropreFin(:,i) = v;
    B = DeflationWielandt(B, ValeurPropre(i), v, u);
    %DEBUG% 
    disp([ ' valeur propre trouv�e ', num2str(ValeurPropre(i)) , ' N� ', num2str(i)]);
    toc
end
%Note : si on veut les vraies valeurs propre, il faudrait les inverser
%(1/lambda) car on prend l'inverse de A ! (inv(A)) ! 

%% On r�cup�re la valeur propre et le vecteur propre les plus proche de "c"
tic
c = 6.82;
Z = inv(A-c*eye(taille(1))); %Voir formule fiche TP2
[ValeurpropreZ, vZ, uZ] = PuissanceIteree(Z);
ValeurpropreC = c + 1 / ValeurpropreZ;

disp([ 'Valeur propre la plus proche de c trouv�e ', num2str(ValeurpropreC)]);
toc

%% Arr�t compteur temps g�n�ral
toc

%% Affichage
v = VideoWriter('video1.avi','Uncompressed AVI');
open(v)
for i=1:20
    surf(reshape(VecteurPropreDebut(:,i),[16,40]));
     M(i)=getframe;
     writeVideo(v,M(i))
end
movie(M,2,1);
close(v)

for i=1:50
    surf(reshape(VecteurPropreFin(:,i),[16,40]));
    N(i)=getframe;
end
movie(N,2,1);