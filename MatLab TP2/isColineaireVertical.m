function [rep] = isColineaireVertical(X, Y, erreur)
%Attention ne v�rifie que les verticaux
rep = 1;
taille = size(X);

for i=1:taille(1)
    if X(i) ~= 0
        coefCourant = Y(i)/X(i);
        break
    end
end

% DEBUG % disp([ ' coefCourant Vertical ', num2str(coefCourant)]);

% Dans chaque case : un 1 sur les deux cases sont identiques, un 0 sinon
% � gauche, un arrondi de X * coefficient multiplicateur / � droite : Y
tabBoolean = (round(X*coefCourant, erreur) == round(Y, erreur));
% Si il y a un seul z�ro, on le r�cup�re
rep = min(tabBoolean);

%%  On peut remplacer les lignes ci-dessous, par les deux expressions ci-dessus, pour acc�l�rer le traitement.
% for i=1:taille(1)
%     if round(X(i)*coefCourant, erreur) ~= round(Y(i), erreur)
%         %round(C(i), erreur) ~= round(coefCourant, erreur) %si coefs trouv�s sont similaire � 10^-3 pr�s
%         rep = 0;
%     end
% end

end