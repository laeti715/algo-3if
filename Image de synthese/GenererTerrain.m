function [carteNiveau , terrainFinal] = GenererTerrain(initterrain, nsubdiv, alpha, lambda)
% Lambda compris entre 0 et 1

terrainFinal=initterrain; 
for i=1:nsubdiv
    terrainFinal = Subdivise(terrainFinal, alpha); 
    alpha = lambda*alpha; 
end

% On r�cup�re la carte de niveaux
carteNiveau  = CourbeNiveau(terrainFinal, 10);

% On r�cup�re la carte de niveaux en couleurs
colorramp = imread('colorramp.png');
carteNiveauCouleur  = CourbeNiveauRampe(terrainFinal, 10, colorramp);

%On rescale jusqu'� un int 16 bits
terrainFinal = reScale(terrainFinal,65535);
terrainFinal = uint16(terrainFinal);

imwrite(terrainFinal, 'terrainFinal.png');

end