function [carteOmbre] = Ombrage( terrain, inter, rampfile, normales, points, lumiere)
% lumiere source ponctuelle
% points ce qui est ressorti par ConstruitsPoints3D

%% On récupère les tailles
tailleTerrain = size(terrain);
h=tailleTerrain(1);
l=tailleTerrain(2);

carteOmbre = zeros(h,l,3);

terrainRescale = reScale(terrain, 489);

%% Pour chaque point : on calcul le vecteur point->source et on utilise le cosinus avec la normale à la surface, de ce point.
for i = 1 : h
    for j = 1 : l
        %% Calcul du vecteur point-> source
        vecteurEclairement = [lumiere(1) - points(i,j,1);lumiere(2) - points(i,j,2);lumiere(1) - points(i,j,3)]; % Dans l'ordre : x, y , z
        % VECTEUR source -> Point / pour tests % vecteurEclairement = [points(i,j,1)-lumiere(1);points(i,j,2)-lumiere(2);points(i,j,3)-lumiere(3)]; % Dans l'ordre : x, y , z
        
        %% Recupération du vecteur normal
        vecteurNormal = [normales(i,j,1);normales(i,j,2);normales(i,j,3)];
        
        %% Calcul du cosinus 
        cosinus = (vecteurEclairement' * vecteurNormal)/ ( norm(vecteurEclairement) * norm(vecteurNormal) );
        % produitScalaire = Norme(v1) * Normae (V2) * cos(angles(V1,V2)
        
        %% Coloriage de cette case du terrain
        if abs( mod(terrain(i,j),inter) ) <= 0.1*inter
            % Si c'est une courbe de niveau
            carteOmbre(i,j,:) = 40; % noir
        else
            %La couleur correspondante à l'échelle, pondéré par le cosinus
            %de l'éclairement
            val = ceil(terrainRescale(i,j))+1; % Car il peut y avoir des zéros !
            carteOmbre(i,j,1) = rampfile(1,val,1)*cosinus;
            carteOmbre(i,j,2) = rampfile(1,val,2)*cosinus;
            carteOmbre(i,j,3) = rampfile(1,val,3)*cosinus;
        end
    end
end

carteOmbre = uint8(carteOmbre);
imwrite(carteOmbre, 'carteOmbre.png');

end