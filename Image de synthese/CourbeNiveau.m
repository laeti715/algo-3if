function [carteNiveau] = CourbeNiveau(terrain, inter)

tailleTerrain = size(terrain);
h=tailleTerrain(1);
l=tailleTerrain(2);

carteNiveau = zeros(h,l,3);

%carteNiveau(abs( mod(terrain,inter) ) <= 0.1*inter,:) = 40

for i = 1 : h
    for j = 1 : l
        if abs( mod(terrain(i,j),inter) ) <= 0.1*inter
            carteNiveau(i,j,:) = 40; % noir
        else
            carteNiveau(i,j,:) = 255; % blanc
        end
    end
end

carteNiveau = uint8(carteNiveau);
imwrite(carteNiveau, 'CourbeNiveau.png');

end