function [terrainSubdivise] = Subdivise(terrain, alpha)

tailleTerrain = size(terrain);
h=tailleTerrain(1);
l=tailleTerrain(2);

terrainSubdivise = zeros(h*2-1, l*2-1);

% points rouges --> dilatation
for i = 1 : h
    for j = 1 : l
        terrainSubdivise(i*2-1,j*2-1) = terrain(i, j);
    end
end

% Points centraux => 4 voisins � chaque fois, modul� d'un facteur entre -alpha et + alpha
for i = 1 : h*2-1
    for j = 1 : l*2-1
        if(mod(i,2) == 0 && mod(j,2) == 0)
            somme = terrainSubdivise(i-1,j-1) + terrainSubdivise(i-1,j+1);
            somme = somme + terrainSubdivise(i+1,j-1) + terrainSubdivise(i+1,j+1);
            alphaCourant = -alpha + (2*alpha).*rand(1,1);
            terrainSubdivise(i,j) = (somme/4) + alphaCourant;
        end
    end
end

%
for i = 1 : h*2-1
    for j = 1 : l*2-1
        
        if((mod(i,2) == 0 || mod(j,2) == 0) && ~(mod(i,2) == 0 && mod(j,2) == 0) )%ou exclusif
            nbVoisins = 0 ;
            somme = 0;
            
            % Sauf Premi�re ligne
            if(i~=1)
                somme = somme + terrainSubdivise(i-1,j);
                nbVoisins = nbVoisins+1;
            end
            
            % Sauf Premi�re colonne
            if(j~=1)
                somme = somme + terrainSubdivise(i,j-1);
                nbVoisins = nbVoisins+1;
            end
            
            %Sauf derni�re ligne
            if(i~=h*2-1)
                somme = somme + terrainSubdivise(i+1,j);
                nbVoisins = nbVoisins+1;
            end
            
            %Sauf derni�re colonne
            if(j~=l*2-1)
                somme = somme +terrainSubdivise(i,j+1);
                nbVoisins = nbVoisins+1;
            end
            
            % On compute la valeur al�atoire
            alphaCourant = -alpha + (2*alpha).*rand(1,1);
            % On ajoute aussi la valeur al�atoire
            terrainSubdivise(i,j) = (somme/nbVoisins) + alphaCourant;
        end
    end
end

end